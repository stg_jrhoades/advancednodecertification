const userData = [
    {
        id: "577620ea-75b3-4675-a516-923d49a1efb8",
        firstname: "Justin",
        lastname: "Rhoades",
        boss: "",
        role: 0
    },
    {
        id: "577620ea-75b3-4675-a516-923d49a1efb1",
        firstname: "Justin",
        lastname: "Anderson",
        boss: "",
        role: 1
    },
    {
        id: "577620ea-75b3-4675-a516-923d49a1efb2",
        firstname: "Justin",
        lastname: "Zen",
        boss: "",
        role: 1
    },
    {
        id: "577620ea-75b3-4675-a516-923d49a1efb3",
        firstname: "Justin",
        lastname: "Rhoades",
        boss: "577620ea-75b3-4675-a516-923d49a1efb1",
        role: 2
    },
    {
        id: "577620ea-75b3-4675-a516-923d49a1efb4",
        firstname: "Justin",
        lastname: "Rhoades",
        boss: "577620ea-75b3-4675-a516-923d49a1efb3",
        role: 3
    },
    {
        id: "577620ea-75b3-4675-a516-923d49a1efb4",
        firstname: "Justin",
        lastname: "Forger",
        boss: "577620ea-75b3-4675-a516-923d49a1efb3",
        role: 3
    }
]


const users = {
    getUsers: () => { return userData },
    insertUser: (user) => {
        userData.push(user);
        return userData;
    },
    deleteUser: (index) => {
        userData.splice(index, 1);
        return userData;
    }
}

module.exports = users;