const jwt = require("jsonwebtoken");

function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const [type, token] = authHeader.split(" ");

    if (type !== "Bearer") return res.sendStatus(401);
    if (token == null) return res.sendStatus(401);

    jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
        if (err) {
            console.error("Error", err);
            return res.sendStatus(403);
        }

        req.user = user;
        next()
    })
}

module.exports = { authenticateToken };