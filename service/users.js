const crypto = require("crypto");
const users = require("../db.js");

// Sorting
const sortByLastName = (arr, sort) => {
    if (sort === "asc") {
        return arr.sort((a, b) => {
            const la = a.lastname.toLowerCase();
            const lb = b.lastname.toLowerCase();

            if (la < lb) return -1;
            if (la > lb) return 1;
            return 0;
        })
    } else {
        return arr.sort((a, b) => {
            const la = a.lastname.toLowerCase();
            const lb = b.lastname.toLowerCase();

            if (la > lb) return -1;
            if (la < lb) return 1;
            return 0;
        })
    }
}

// Paging
const pageing = (arr, start, limit) => {
    if (isNaN(start) || isNaN(limit)) {
        throw new Error("not a number");
    }

    const end = parseInt(start) + parseInt(limit);
    return arr.slice(parseInt(start), end);
}

// CRUD operations for users
const getUsers = async () => {
    return users.getUsers();
}

const getUser = async (id) => {
    const user = users.getUsers().filter(user => user.id === id)[0];
    if (!user) {
        throw new Error("no user");
    }
    return user;
}

const createUser = async (user) => {
    const uuid = crypto.randomUUID();
    const newUser = {
        id: uuid.toString(),
        ...user,
        boss: ""
    }
    users.insertUser(newUser);
    return  newUser;
}

const updateUser = async (id, data) => {
    const oldUser = users.getUsers().filter(user => user.id === id)[0];
    if (!oldUser) {
        throw new Error("no user")
    }

    await deleteUser(id).catch(err => { throw  err });

    const user = {
        id,
        firstname: data.firstname || oldUser.firstname,
        lastname: data.lastname || oldUser.lastname,
        boss: data.boss || oldUser.boss,
        role: data.role ?? oldUser.role
    }

    users.insertUser(user);

    return user;
}

const deleteUser = async (id) => {
    const index = users.getUsers().findIndex(user => user.id === id);
    if (index === -1) {
        throw new Error("does not exist");
    }

    users.deleteUser(index);
}

module.exports = {
    sortByLastName,
    pageing,
    getUser,
    getUsers,
    createUser,
    updateUser,
    deleteUser
}