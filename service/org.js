const {getUser, getUsers, updateUser} = require("./users.js");

const getOrg = async () => {
    const users = await getUsers().catch(err => { throw err });
    const org = {};

    users.forEach(user => {
        const userRole = user.role.toString();
        org[userRole]
            ? org[userRole].push(user.id)
            : org[userRole] = [user.id];
    });

    return org;
}

const getCommandChain = async (id) => {
    const getBoss = async (bossId) => {
        const user = await getUser(bossId).catch(err => { throw err });
        return {
            id: user.id,
            name: `${user.firstname} ${user.lastname}`,
            role: user.role,
            boss: user.boss !== "" ? await getBoss(user.boss) : ""
        }
    }
    return await getBoss(id).catch(err => { throw err });
}

const changeBoss = async (employeeId, bossId) => {
    return await updateUser(employeeId, { boss: bossId }).catch(err => { throw err });
}

module.exports = {
    getOrg,
    getCommandChain,
    changeBoss
}