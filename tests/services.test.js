const {sortByLastName, pageing, getUsers, getUser, createUser, deleteUser, updateUser} = require("../service/users");
const db = require("../db");
const {getOrg, getCommandChain, changeBoss} = require("../service/org");

const testData = [
   {
      id: "577620ea-75b3-4675-a516-923d49a1efb8",
      firstname: "Justin",
      lastname: "Rhoades",
      boss: "",
      role: 1
   },
   {
      id: "577620ea-75b3-4675-a516-923d49a1efb9",
      firstname: "Sean",
      lastname: "Rhoades",
      boss: "",
      role: 1
   },
   {
      id: "577620ea-75b3-4675-a516-923d49a1efb1",
      firstname: "Neo",
      lastname: "Anderson",
      boss: "577620ea-75b3-4675-a516-923d49a1efb9",
      role: 2
   },
   {
      id: "577620ea-75b3-4675-a516-923d49a1efb2",
      firstname: "Alice",
      lastname: "Zen",
      boss: "577620ea-75b3-4675-a516-923d49a1efb1",
      role: 3
   }
]

jest.mock("../db.js", () => {
   return {
      getUsers: jest.fn(() => {
         return testData
      }),
      insertUser: jest.fn(() => {}),
      deleteUser: jest.fn(() => {})
   }
});

describe("Services", () => {
   describe("User Services", () => {
      beforeEach(() => {
         jest.clearAllMocks();
      })

      it("should sort asc by lastname", async () => {
         const sorted = sortByLastName(testData, "asc");
         expect(sorted.length).toEqual(4);
         expect(sorted[0].lastname).toEqual("Anderson");
         expect(sorted[3].lastname).toEqual("Zen");
      });
      it("should sort desc by lastname", async () => {
         const sorted = sortByLastName(testData, "desc");
         expect(sorted.length).toEqual(4);
         expect(sorted[0].lastname).toEqual("Zen");
         expect(sorted[3].lastname).toEqual("Anderson");
      });
      it("should return paging array", async () => {
         const paged = pageing(testData, 2, 2);
         expect(paged.length).toEqual(2);
         expect(paged[0].firstname).toEqual("Sean");
         expect(paged[1].firstname).toEqual("Neo");
      });
      it("should fail to return paging array", async () => {
         try {
            pageing(testData, 0, "bob");
         } catch (e) {
            expect(e).not.toBeNull();
         }
      });
      it("should get users", async () => {
         const users = await getUsers();
         expect(users).toEqual(testData);
      });
      it("should get specific user", async () => {
         const user = await getUser("577620ea-75b3-4675-a516-923d49a1efb2");
         expect(user.firstname).toEqual("Alice");
         expect(user.lastname).toEqual("Zen");
      });
      it("should fail to get specific user", async () => {
         const user = await getUser("id").catch(err => {
            expect(err).not.toBeNull()
         });
      });
      it("should create a new user", async () => {
         const user = await createUser({
            firstname: "test",
            lastname: "user",
            role: 4
         });

         expect(user.id).not.toBeNull();
         expect(user.firstname).toEqual("test")
         expect(user.lastname).toEqual("user")
         expect(user.boss).toEqual("")
      });
      it("should delete a user", async () => {
         await deleteUser("577620ea-75b3-4675-a516-923d49a1efb8");
         expect(db.deleteUser.mock.calls.length).toEqual(1);
      });
      it("should fail to delete a user", async () => {
         await deleteUser("0").catch(err => {
            expect(err).not.toBeNull();
         });
      });
      it("should update a user", async () => {
         const user = await updateUser("577620ea-75b3-4675-a516-923d49a1efb1", {
            firstname: "Jake",
            lastname: "Neo",
            role: 1
         });

         expect(db.getUsers.mock.calls.length).toEqual(2);
         expect(db.deleteUser.mock.calls.length).toEqual(1);
         expect(db.insertUser.mock.calls.length).toEqual(1);
         expect(user.firstname).toEqual("Jake");
      });
      it("should fail to update a new user", async () => {
         await updateUser("0", {
            firstname: "Jake",
            lastname: "Neo",
            role: 1
         }).catch(err => {
            expect(err).not.toBeNull();
         });
      });
   })

   describe("Org Services", () => {
      beforeEach(() => { jest.clearAllMocks() });
      it("should get org", async () => {
         const org = await getOrg();
         expect(org[1].length).toEqual(2)
         expect(org[2].length).toEqual(1)
         expect(org[3].length).toEqual(1)
      });
      it("should get user's command chain", async () => {
         const data = await getCommandChain("577620ea-75b3-4675-a516-923d49a1efb2");
         expect(data.boss).not.toEqual("");
         expect(data.boss.boss).not.toEqual("");
         expect(data.boss.boss.boss).toEqual("");
      });
      it("should fail to get user's command chain", async () => {});
      it("should change boss", async () => {
         const user = await changeBoss("577620ea-75b3-4675-a516-923d49a1efb2", "577620ea-75b3-4675-a516-923d49a1efb8");
         expect(user.boss).toEqual("577620ea-75b3-4675-a516-923d49a1efb8");
      });
   })
})