const request = require("supertest");
const app = require("../app");
const {createUser, updateUser, deleteUser} = require("../service/users");
const {getCommandChain, changeBoss} = require("../service/org");

// Mock all the users services
jest.mock("../service/users", () => {
    return {
        sortByLastName: jest.fn((arr, sort) => { if (sort === "asc") {
            return arr.sort((a, b) => {
                const la = a.lastname.toLowerCase();
                const lb = b.lastname.toLowerCase();

                if (la < lb) return -1;
                if (la > lb) return 1;
                return 0;
            })
        } else {
            return arr.sort((a, b) => {
                const la = a.lastname.toLowerCase();
                const lb = b.lastname.toLowerCase();

                if (la > lb) return -1;
                if (la < lb) return 1;
                return 0;
            })
        } }),
        pageing: jest.fn((arr, start, limit) => {
            if (isNaN(start) || isNaN(limit)) {
                throw new Error("not a number");
            }

            const end = parseInt(start) + parseInt(limit);
            return arr.slice(parseInt(start), end);
        }),
        getUsers: jest.fn(async () => {
            return Promise.resolve([
                {
                    id: "577620ea-75b3-4675-a516-923d49a1efb8",
                    firstname: "Justin",
                    lastname: "Rhoades",
                    boss: "",
                    role: 1
                },
                {
                    id: "577620ea-75b3-4675-a516-923d49a1efb1",
                    firstname: "Neo",
                    lastname: "Anderson",
                    boss: "",
                    role: 2
                },
                {
                    id: "577620ea-75b3-4675-a516-923d49a1efb2",
                    firstname: "Alice",
                    lastname: "Zen",
                    boss: "",
                    role: 3
                }
            ])
        }),
        getUser: jest.fn(async () => {
            return Promise.resolve({
                id: "577620ea-75b3-4675-a516-923d49a1efb8",
                firstname: "Justin",
                lastname: "Rhoades",
                boss: "",
                role: 1
            })
        }),
        createUser: jest.fn(async (user) => {
            if(user.firstname !== "justin") {
                await Promise.resolve();
            } else {
                await Promise.reject();
            }
        }),
        updateUser: jest.fn(async (id, user) => {
            if(user.firstname !== "justin") {
                await Promise.resolve();
            } else {
                await Promise.reject();
            }
        }),
        deleteUser: jest.fn(async () => {})
    }
});


// Org mocks
jest.mock("../service/org", () => {
    return {
        getOrg: jest.fn(async () => {}),
        getCommandChain: jest.fn(async (id) => {
            if (id === "0") {
                await Promise.reject()
            }
            else if (id === "1") {
                await Promise.reject("no user");
            } else {
                await Promise.resolve();
            }
        }),
        changeBoss: jest.fn(async (eId, bId) => {
            if (eId === "0") {
                await Promise.reject()
            }
            else if (bId === "1") {
                await Promise.reject("no user");
            } else {
                await Promise.resolve();
            }
        })
    }
});


// Testing
describe("Routes", () => {
    let token;
    const cUser = {
        firstname: "Mick",
        lastname: "Rhoades",
        role: 1
    }
    const uUser = {
        firstname: "",
        lastname: "",
        role: 1
    }

    beforeAll(async () => {
        const res = await request(app)
            .post("/auth/login")
            .send({email: "admin@example.com", password: "secret"});

        expect(res.body.token).not.toBeNull();
        token = res.body.token;
    })

    describe('Auth Endpoint', () => {
        it("Should prevent login", async () => {
            const res = await request(app)
                .post("/auth/login")
                .send({email: "", password: "secret"});

            expect(res.statusCode).toEqual(400);
        })

        it("Should prevent login 2", async () => {
            const res = await request(app)
                .post("/auth/login")
                .send({email: "admin@example.com", password: ""});

            expect(res.statusCode).toEqual(400);
        })
    });

    describe('Auth Middleware', () => {
        it("Should prevent getting specific user", async () => {
            const res = await request(app)
                .get("/users/577620ea-75b3-4675-a516-923d49a1efb8")
                .set("Authorization", `Bearer null`);

            expect(res.statusCode).toEqual(403);
        })

        it('should create a new user', async () => {
            const res = await request(app)
                .post("/users")
                .set("Authorization", `${token}`)
                .send(cUser);

            expect(res.statusCode).toEqual(401);
        });
    });

    describe("User Routes", () => {
        it("Should return users", async () => {
            const res = await request(app)
                .get("/users/")
                .set("Accept", "application/json");

            expect(res.statusCode).toEqual(200);
            expect(res.body.length).toEqual(3);
        });

        it("Should return sorted users", async () => {
            const res = await request(app)
                .get("/users?sort=asc&page=1&limit=2")
                .set("Authorization", `Bearer ${token}`);


            expect(res.statusCode).toEqual(200);
            expect(res.body[0].lastname).toEqual("Rhoades")
            expect(res.body.length).toEqual(2);
        });

        it("Should fail return sorted users", async () => {
            const res = await request(app)
                .get("/users?sort=asc&page=1&limit=bob")
                .set("Authorization", `Bearer ${token}`);


            expect(res.statusCode).toEqual(400)
        });

        it("Should get specific user", async () => {
            const res = await request(app)
                .get("/users/577620ea-75b3-4675-a516-923d49a1efb8")
                .set("Authorization", `Bearer ${token}`);

            expect(res.statusCode).toEqual(200);
            expect(res.body.lastname).toEqual("Rhoades")
        });

        it('should create a new user', async () => {
            const res = await request(app)
                .post("/users")
                .set("Authorization", `Bearer ${token}`)
                .send(cUser);

            expect(res.statusCode).toEqual(200);
            expect(createUser.mock.calls.length).toEqual(1);
        });

        it('should fail create a new user', async () => {
            const res = await request(app)
                .post("/users")
                .set("Authorization", `Bearer ${token}`)
                .send({
                    ...cUser,
                    firstname: "justin"
                });

            expect(res.statusCode).toEqual(500);
        });

        it('should fail create a new user', async () => {
            const res = await request(app)
                .post("/users")
                .set("Authorization", `Bearer ${token}`)
                .send({
                    ...cUser,
                    firstname: undefined
                });

            expect(res.statusCode).toEqual(400);
        });

        it('should update a user', async () => {
            const res = await request(app)
                .put("/users/577620ea-75b3-4675-a516-923d49a1efb1")
                .set("Authorization", `Bearer ${token}`)
                .send(uUser);

            expect(res.statusCode).toEqual(200);
            expect(updateUser.mock.calls.length).toEqual(1);
        });

        it('should fail update a user', async () => {
            const res = await request(app)
                .put("/users/577620ea-75b3-4675-a516-923d49a1efb1")
                .set("Authorization", `Bearer ${token}`)
                .send({
                    ...uUser,
                    firstname: "justin"
                });

            expect(res.statusCode).toEqual(500);
        });

        it('should fail to update a user 2', async () => {
            const res = await request(app)
                .put("/users/577620ea-75b3-4675-a516-923d49a1efb1")
                .set("Authorization", `Bearer ${token}`)
                .send({
                    ...uUser,
                    boss: "something"
                });

            expect(res.statusCode).toEqual(400);
        });

        it('should fail to update a user 3', async () => {
            const res = await request(app)
                .put("/users/577620ea-75b3-4675-a516-923d49a1efb1")
                .set("Authorization", `Bearer ${token}`)
                .send({
                    ...uUser,
                    lastname: undefined
                });

            expect(res.statusCode).toEqual(400);
        });

        it("Should delete specific user", async () => {
            const res = await request(app)
                .delete("/users/577620ea-75b3-4675-a516-923d49a1efb8")
                .set("Authorization", `Bearer ${token}`);

            expect(res.statusCode).toEqual(200);
            expect(deleteUser.mock.calls.length).toEqual(1);
        });
    });

    describe("Org Routes", () => {
        it("should get org chart", async () => {
            const res = await request(app)
                .get("/org/")
                .set("Authorization", `Bearer ${token}`);

            expect(res.statusCode).toEqual(200);
        });

        it("should get user's command chain", async () => {
            const res = await request(app)
                .get("/org/577620ea-75b3-4675-a516-923d49a1efb8")
                .set("Authorization", `Bearer ${token}`);

            expect(res.statusCode).toEqual(200);
            expect(getCommandChain.mock.calls.length).toEqual(1);
        });

        it("should fail to get user's command chain 2", async () => {
            const res = await request(app)
                .get("/org/0")
                .set("Authorization", `Bearer ${token}`);

            expect(res.statusCode).toEqual(500);
        });

        it("should fail to get user's command chain 3", async () => {
            const res = await request(app)
                .get("/org/1")
                .set("Authorization", `Bearer ${token}`);

            expect(res.statusCode).toEqual(404);
        });

        it("should change boss", async () => {
            const res = await request(app)
                .put("/org")
                .set("Authorization", `Bearer ${token}`)
                .send({ employee: "1", boss: "2" });

            expect(res.statusCode).toEqual(200);
            expect(changeBoss.mock.calls.length).toEqual(1);
        });

        it("should fail change boss 1", async () => {
            const res = await request(app)
                .put("/org")
                .set("Authorization", `Bearer ${token}`)
                .send({ employee: "0", boss: "2" });

            expect(res.statusCode).toEqual(500);
        });

        it("should fail change boss 2", async () => {
            const res = await request(app)
                .put("/org")
                .set("Authorization", `Bearer ${token}`)
                .send({ employee: "1", boss: "1" });

            expect(res.statusCode).toEqual(400);
        });

        it("should fail change boss 3", async () => {
            const res = await request(app)
                .put("/org")
                .set("Authorization", `Bearer ${token}`)
                .send({ employee: "", boss: "1" });

            expect(res.statusCode).toEqual(400);
        });
    });
})