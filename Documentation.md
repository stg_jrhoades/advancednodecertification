# Documentation

## How to run

1. Run `npm i`
2. After all dependencies are installed create a `.env` file for the token secret
3. Run `npm start`


## Auth Endpoints

### Login

method: `POST`

uri: `/auth/login`

request

```json
{
  "email": "admin@example.com",
  "password": "N0t!mport@nt"
}
```

response

```json
{
  "token": "<jwt token>"
}
```

errors
```json
status code: 500
status code: 400
```

## Employee Endpoints

### Get Employees

get list of employees (users) on the system

method: `GET`

uri: `/users`

query parameters: `?sort=asc&page=0&limit=3`
- sort: asc or desc
- page: starting point
- limit: how many entries per page

response

```json
[
  {
    "id": "577620ea-75b3-4675-a516-923d49a1efb8",
    "firstName": "justin",
    "lastName": "rhoades",
    "boss": "577620ea-75b3-4675-a516-923d49a1efb2",
    "role": 0
  }
  ...
]
```

errors
```json
status code: 500
```

### Get Employee

get employee (users) on the system

method: `GET`

uri: `/users/:id`

auth:

```json
{
  "Authorization": "Bearer <token>"
}
```

response

```json
{
  "id": "577620ea-75b3-4675-a516-923d49a1efb8",
  "firstName": "justin",
  "lastName": "rhoades",
  "boss": "577620ea-75b3-4675-a516-923d49a1efb2",
  "role": 0
}
```

errors

```json
status code: 500
status code: 401
status code: 404
{
  error: "no user"
}
```

### Create Employee

create a new employee on the system

method: `POST`

uri: `/users`

auth:

```json
{
  "Authorization": "Bearer <token>"
}
```

request

```json
{
  "firstname": "justin",
  "lastname": "rhoades",
  "role": 0
}
```

response

```json
{
  "id": "577620ea-75b3-4675-a516-923d49a1efb8",
  "firstname": "justin",
  "lastname": "rhoades",
  "boss": "",
  "role": 0
}
```

errors

```json
status code: 500
status code: 401
status code: 400
{
  error: "bad data"
}
```

### Update Employee

update a new employee on the system

method: `PUT`

uri: `/users/:id`

auth:

```json
{
  "Authorization": "Bearer <token>"
}
```

request

```json
{
  "firstname": "sean",
  "lastname": "",
  "role": null
}
```

response

```json
{
  "id": "577620ea-75b3-4675-a516-923d49a1efb8",
  "firstname": "sean",
  "lastname": "rhoades",
  "boss": "577620ea-75b3-4675-a516-923d49a1efb2",
  "role": 3
}
```

errors
```json
status code: 500
status code: 401
status code: 404
{
  error: "no user"
}
status code: 400
{
  error: "bad data"
}
```

### Delete Employee

delete employee (users) on the system

method: `DELETE`

uri - `/users/:id`

auth:

```json
{
  "Authorization": "Bearer <token>"
}
```

response: `200`

errors

```json
status code: 500
status code: 401
status code: 400
{
  error: "bad data"
}
status code: 404
{
  error: "no user"
}
```

## Org Endpoints

### Org Chart
```text
Roles
0 = Admin
1 = Chiefs
2 = VP
3 = Managers
4 = Employees
```

### Get Org Chart

Get the role breakdown and hierarchy

method: `GET`

uri: `/org`

auth:

```json
{
  "Authorization": "Bearer <token>"
}
```

response
```json
[
  {
    "role": 0,
    "memebers": [
      "577620ea-75b3-4675-a516-923d49a1efb8"
    ]
  },
  {
    "role": 1,
    "memebers": [
      "577620ea-75b3-4675-a516-923d49a1efb8",
      "577620ea-75b3-4675-a516-923d49a1efb8",
      "577620ea-75b3-4675-a516-923d49a1efb8"
    ]
  },
  ...
]
```

errors
```json
status code: 500
status code: 401
```

### Get employees chain of command

method: `GET`

uri: `/org/:id`

auth:

```json
{
  "Authorization": "Bearer <token>"
}
```

response
```json
{
  "id": "577620ea-75b3-4675-a516-923d49a1efb8",
  "name": "Justin Rhoades",
  "role": 4,
  "boss": {
    "id": "577620ea-75b3-4675-a516-923d49a1efb8",
    "name": "Justin Rhoades",
    "role": 3,
    "boss": {
      "id": "577620ea-75b3-4675-a516-923d49a1efb8",
      "name": "Justin Rhoades",
      "role": 2,
      "boss": {
        "id": "577620ea-75b3-4675-a516-923d49a1efb8",
        "name": "Justin Rhoades",
        "role": 1,
        "boss": {}
      }
    }
  }
}
```

errors
```json
status code: 500
status code: 401
status code: 404
{
  error: "no user"
}
```

### Change Boss

method: `PUT`

uri: `/org`

auth:

```json
{
  "Authorization": "Bearer <token>"
}
```

request

```json
{
  "employee": "577620ea-75b3-4675-a516-923d49a1efb8",
  "boss": "577620ea-75b3-4675-a516-923d49a1efb2"
}
```

response
```json
{
  "id": "577620ea-75b3-4675-a516-923d49a1efb8",
  "firstName": "justin",
  "lastName": "rhoades",
  "boss": "577620ea-75b3-4675-a516-923d49a1efb2",
  "role": 0
}
```

errors
```json
status code: 500
status code: 401
status code: 400
{
  error: "bad data"
}
```