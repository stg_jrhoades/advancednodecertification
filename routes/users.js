const router = require("express").Router();

const {getUsers, createUser, getUser, updateUser, deleteUser, sortByLastName, pageing} = require("../service/users.js");
const {authenticateToken} = require("../auth.js");

const isValidUser = (data) => {
    if (data === undefined) throw new Error("no body");
    if (data.firstname === undefined) throw new Error("need first name");
    if (data.lastname === undefined) throw new Error("need last name");
    if (data.role === undefined) throw new Error("need role");
}

/* GET users listing. */
router.get("/", (req, res, next) => {
    const { sort, page, limit } = req.query;
    getUsers()
        .then(users => {
            try {
                if (sort) {
                    users = sortByLastName(users, sort);
                }

                if (page) {
                    users = pageing(users, page, limit ?? 5);
                }
            } catch (e) {
                res.status(400).send({ error: "bad data" });
                return;
            }

            res.send(users);
        })
        .catch(err => { res.status(500).send({error: err}) });
});

/* GET user. */
router.get("/:id", authenticateToken, (req, res, next) => {
    getUser(req.params.id)
        .then(user => { res.send(user); })
        .catch(err => {
            if (err === "no user") {
                res.status(404).send({error: err})
            }
            res.status(500).send({error: err})
        });
});

/*POST create user */
router.post("/", authenticateToken, (req, res,  next) => {
    const reqUser = req.body;
    try {
        isValidUser(reqUser)
    } catch (err) {
        res.status(400).send({ error: "bad data" });
        return;
    }

    createUser(reqUser)
        .then(newUser => { res.send(newUser) })
        .catch(err => { res.status(500).send({ error: err }) });
})

/*PUT update user */
router.put("/:id", authenticateToken, (req, res,  next) => {
    const reqUser = req.body;
    try {
        isValidUser(reqUser)
        if (reqUser.boss !== undefined) {
            res.status(400).send({ error: "bad data" });
            return;
        }
    } catch (err) {
        res.status(400).send({ error: "bad data" });
        return;
    }

    updateUser(req.params.id, reqUser)
        .then(newUser => { res.send(newUser) })
        .catch(err => {
            if (err === "no user") {
                res.status(404).send({ error: err })
                return;
            }
            res.status(500).send({ error: err })
        });
});

/* DELETE user. */
router.delete("/:id", authenticateToken, (req, res, next) => {
    deleteUser(req.params.id)
        .then(user => { res.send(user); })
        .catch(err => { res.status(404).send({error: err}) });
});


module.exports = router;
