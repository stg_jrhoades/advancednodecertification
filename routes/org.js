const router = require("express").Router();
const {changeBoss, getCommandChain, getOrg} = require("../service/org.js");
const {authenticateToken} = require("../auth.js");

/* GET users listing. */
router.get("/", authenticateToken, (req, res, next) => {
    getOrg()
        .then(org => { res.send(org) })
        .catch(err => { res.status(500).send({ error: err }) });
});

/* GET employee chain of command */
router.get("/:id", authenticateToken, (req, res, next) => {
    getCommandChain(req.params.id)
        .then(cc => { res.send(cc) })
        .catch(err => {
            if (err === "no user") {
                res.status(404).send({ error: err })
                return;
            }
            res.status(500).send({ error: err })
        });
});

/* PUT change boss */
router.put("/", authenticateToken, (req, res, next) => {
    const body = req.body;
    if (
        body.employee === undefined
        || body.employee === ""
        || body.boss === undefined
        || body.boss === ""
    ) {
        res.status(400).send({ error: "bad data" });
        return;
    }

    changeBoss(body.employee, body.boss)
        .then(user => { res.send(user) })
        .catch(err => {
            if (err === "no user") {
                res.status(400).send({ error: err })
                return;
            }
            res.status(500).send({ error: err })
        });

})



module.exports = router;
